%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

%global target msp430-elf
%global ti_version_number 9_3_1_2

Name:		msp430-gcc-support-files
Version:	1.212
Release:	2%{?dist}
Summary:	Headers for MSP430 micro-controllers
Group:		Development/Libraries
License:	BSD-3-Clause
BuildArch:	noarch
URL:		https://www.ti.com/tool/MSP430-GCC-OPENSOURCE
Source0:	http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/%{ti_version_number}/export/msp430-gcc-support-files-%{version}.zip

%description
Headers and for the Texas Instruments MSP430 range of
micro-controllers. These headers provide the location of all the
special function registers of the micro-controllers.

%prep
%autosetup -c %{name}

%build
# No build to do

%install
mkdir -p %{buildroot}%{_prefix}/%{target}/%{target}/include
mkdir -p %{buildroot}%{_prefix}/%{target}/%{target}/lib
cp -a msp430-gcc-support-files/include/*.h %{buildroot}%{_prefix}/%{target}/%{target}/include
cp -a msp430-gcc-support-files/include/devices.csv %{buildroot}%{_prefix}/%{target}/%{target}/include
cp -a msp430-gcc-support-files/include/*.ld %{buildroot}%{_prefix}/%{target}/%{target}/lib

%files
%dir %{_prefix}/%{target}
%dir %{_prefix}/%{target}/%{target}
%dir %{_prefix}/%{target}/%{target}/include
%{_prefix}/%{target}/%{target}/include/*.h
%{_prefix}/%{target}/%{target}/include/devices.csv
%dir %{_prefix}/%{target}/%{target}/lib
%{_prefix}/%{target}/%{target}/lib/*.ld

%changelog
* Fri Sep 15 2023 Brandon Nielsen <nielsenb@jetfuse.net> - 1.212-2
- Change to SPDX license field

* Wed Aug 11 2021 Brandon Nielsen <nielsenb@jetfuse.net> - 1.212-1
- Update to 1.212

* Mon Nov 16 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 1.210-3
- Match the packaging guidelines by placing files in /usr/msp430-elf
- Mark package noarch since system libdir is no longer used
- Update URL
- Update source URL and TI version number
- Own created directories

* Tue Jul 14 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 1.210-2
- Use /usr/include/msp430-elf for headers
- Don't use architecture specific libdir, use /usr/lib/msp430-elf

* Sat Jul 11 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 1.210-1
- Update to 1.210
- Add dist to release

* Sat Jul 11 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 1.210-1
- Update to 1.210
- Add dist to release

* Mon Sep 30 2019 Brandon Nielsen <nielsenb@jetfuse.net> - 1.207-1
- Update to 1.207

* Mon Jul 2 2018 Brandon Nielsen <nielsenb@jetfuse.net> - 1.205-1
- Update to 1.205

* Fri Mar 30 2018 Brandon Nielsen <nielsenb@jetfuse.net> - 1.204-1
- Update to 1.204

* Fri Jan 12 2018 Brandon Nielsen <nielsenb@jetfuse.net> - 1.203-1
- Update to 1.203
- Remove unecessary cleanup

* Sun Oct 29 2017 Brandon Nielsen <nielsenb@jetfuse.net> - 1.201-1
- Update to 1.201
- Add devices.csv

* Tue May 2 2017 Brandon Nielsen <nielsenb@jetfuse.net> - 1.198-1
- Switch to TI / SOMNIUM Technology upstream

* Thu Dec 1 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-2
- Remove dist from release

* Tue Oct 27 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-1
- Update to 3.5.0.0

* Thu Oct 15 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.4.5.1-3
- Disable debug packages for building on Fedora 23

* Fri Aug 14 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.4.5.1-2
- Put libraries in libdir depending on architecture

* Fri Aug 14 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.4.5.1-1
- Update to 3.4.5.1

* Tue Jun 2 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.3.4-1
- Update to 3.3.4

* Mon Feb 23 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.2.3-1
- Update to 3.2.3

* Sun Jan 25 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.2.2
- Change to official TI sources

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20120406-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20120406-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20120406-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Oct 07 2012 Rob Spanton <rspanton@zepler.net> 20120406-5
- Bump release number to fix upgrade path from F17

* Tue Aug 14 2012 Rob Spanton <rspanton@zepler.net> 20120406-3
- Remove %%clean section
- Remove %%defattr line
- Remove rm -rf %%{buildroot} from %%install section

* Fri May 18 2012 Rob Spanton <rspanton@zepler.net> 20120406-2
- Complete summary and description fields

* Wed May 16 2012 Rob Spanton <rspanton@zepler.net> 20120406-1
- Initial release
