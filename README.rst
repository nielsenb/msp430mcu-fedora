=================================
 msp430-gcc-support-files-fedora
=================================

Specfile for packaging the TI / Redhat MSP430 support files.

See the official `TI`_ page for more details.

RPM is available at the `msp430mcu copr repository`_.

.. _TI: http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/index_FDS.html
.. _msp430mcu copr repository: https://copr.fedoraproject.org/coprs/nielsenb/msp430mcu/
